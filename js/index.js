const gun = Gun()
const dbTable = 'items'

class FormElement extends HTMLElement {
    connectedCallback() {
	this.render()
    }

    handleChange = (event) => {
	this[event.target.name] = event.target.value
    }

    handleSubmit = (event) => {
	if (!this.title) return

	let data = {}
	data.title = this.title
	gun.get(dbTable).set(data)
    }
    render = () => {
	const $form = document.createElement('form')
	$form.addEventListener('submit', event => {
	    event.preventDefault()
	    this.handleSubmit(event)
	})

	const $title = document.createElement('input')
	$title.title = 'title'
	$title.name = 'title'
	$title.placeholder = 'title'
	$title.value = this.title
	$title.oninput = this.handleChange

	const $button = document.createElement('button')
	$button.innerText = 'add'
	$button.type = 'submit'

	$form.append($title)
	$form.append($button)
	this.append($form)
    }
}

class ListElement extends HTMLElement {
    connectedCallback() {
	this.items = []
	gun.get(dbTable).map().on(item => {
	    const sItem = this.serializeItem(item)
	    let elExists = this.items.filter(el => {
		if (!el) return false
		console.log('el.id === sItem.id', el.id === sItem.id)
		return el.id === sItem.id
	    }).length

	    console.log('elExists', elExists)

	    if (elExists) return

	    this.items.push(sItem)
	    this.render()
	})
    }
    serializeItem(item) {
	if (!item || !item['_'] || !item['_']['#']) return

	let data = {}
	data.id = item['_']['#']
	if (item.title) {
	    data.title = item.title
	}

	return data
    }
    render() {
	if (!this.items || !this.items.length) return
	console.log('data', this.items)
	
	this.items.forEach(item => {
	    if (!item || !item.title) return
	    const $item = document.createElement('p')
	    $item.innerText = item.title
	    this.append($item)
	})
    }
}

customElements.define('form-element', FormElement)
customElements.define('list-element', ListElement)


export default {
    FormElement,
    ListElement
}
